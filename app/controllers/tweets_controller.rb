# encoding = utf-8
class TweetsController < ApplicationController
  
  def new
  	@tweet = Tweet.new
  end

  def default    
  end
  
  def index
  end

  def show
  end

  # -----------------
  # CREATE
  # El método create recibe params = { tweet: { monstruo_id: valor, estado: valor } }
  #punto4
  def create
    tweet = Tweet.new(params.require(:tweet).permit(:monstruo_id, :estado))
    if tweet.save
      redirec_to tweets_path, notice: "tweet guardado correctamente"
    else
      flash[:error]= "hubo un error al cargarlo"
      render :new
    end
  end
end
